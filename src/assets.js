const appIcon = require('./assets/appicon.jpg');
const qr = require('./assets/qr.png');
const back = require('./assets/back_arrow.png');
const instruction = require('./assets/instructions.png');
const gradient = require('./assets/gradient.png');
const gradient_button = require('./assets/gradient_button.png');

export {
    appIcon, qr, back, instruction, gradient, gradient_button
}